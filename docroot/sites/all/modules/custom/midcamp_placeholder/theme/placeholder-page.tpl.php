<?php
/**
 * @file
 * Template for placeholder-page element type.
 * Based on http://codyhouse.co/gem/intro-page-full-width-navigation/.
 */
?>
<main id="cd-main-content">
  <section id="cd-intro">
    <header class="cd-header">

      <?php print drupal_render($element['content']); ?>
    </header>
  </section>
  <!-- cd-intro -->
</main>

<div class="cd-shadow-layer"></div>
<footer class="cd-footer">
  <h1>March 19-22, 2015, at UIC.</h1>
  <p>Please <a href="mailto:info@midcamp.org">email us</a> if you have any
    questions about MidCamp or find us on <a
      href="http://twitter.com/midwestcamp">Twitter</a>, <a
      href="http://facebook.com/midcamp">Facebook</a>, and <a
      href="http://plus.google.com/+MidCampOrg">Google+</a>.</p>
</footer>
