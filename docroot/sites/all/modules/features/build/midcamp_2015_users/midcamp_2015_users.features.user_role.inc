<?php
/**
 * @file
 * midcamp_2015_users.features.user_role.inc
 */

/**
 * Implements hook_user_default_roles().
 */
function midcamp_2015_users_user_default_roles() {
  $roles = array();

  // Exported role: Administrator.
  $roles['Administrator'] = array(
    'name' => 'Administrator',
    'weight' => 2,
  );

  return $roles;
}
