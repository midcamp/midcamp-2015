<?php
/**
 * @file
 * midcamp_2015_views.features.inc
 */

/**
 * Implements hook_views_api().
 */
function midcamp_2015_views_views_api($module = NULL, $api = NULL) {
  return array("api" => "3.0");
}
