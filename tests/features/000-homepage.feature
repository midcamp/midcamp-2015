Feature: Hoempage
  In order to find out information about MidCamp
  As a user of the internet
  I need to get to the site's homepage

  Scenario:
    When I go to the homepage
    Then I should see "midcamp"
